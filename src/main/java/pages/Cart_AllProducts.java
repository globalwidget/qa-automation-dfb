package pages;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class Cart_AllProducts extends ApplicationKeywords {

	private static final String getAllProducts = "All Products #xpath=//div[@class='product-item-info']//span[contains(@class,'product-image-container')]";
	private static final String title_selectedProduct_PGP = "Title of Selected Product - PGP #xpath=//strong//a[contains(@class,'product-item')]";
	private static final String addToCart = "Add To Cart #xpath=//span[contains(text(),'Add to Cart')]/..";
	private static final String addToComparisonList = "Add To Comparison List #xpath=//a[contains(@class,'tocompare')]";
	private static final String addToWishList = "Add To Comparison List #xpath=//a[contains(@class,'towishlist')]";
	private static final String shoppingCartLink = "Shopping Cart Link #xpath=//a[contains(text(),'shopping cart')]";
	private static final String title_selectedProduct_PDP = "Title of product selected - PDP #xpath=//h1[contains(@class,'title')]";
	private static final String quantityUpdateBox = "Quantity Update Box #xpath=//div[contains(@class,'control')]/input[@type='number']";
	private static final String productPrice = "Product Price #xpath=//span[contains(@id,'product-price')]/span";
	private static final String comparsionListLink = "Comparison List Link #xpath=//a[contains(text(),'comparison list')]";
	private static final String productAddedtoCartMessage = "Add To Cart Success Message #xpath=//div/div[contains(text(),'added')]";

	public Cart_AllProducts(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to select the first product on the page
	 */
	public void selectFirstProduct() {
		try {
			waitForElementToDisplay(getAllProducts, 10);
			if (isElementDisplayed(getAllProducts)) {
				java.util.List<WebElement> allProducts = new ArrayList<WebElement>();
				allProducts = findWebElements(getAllProducts);
				scrollToViewElement(allProducts.get(0));
				waitTime(3);
				allProducts.get(0).click();
				testStepInfo("The first product is clicked");
			} else {
				testStepFailed("Products were not displayed");
			}
		} catch (Exception e) {
			testStepFailed("First Product could not be selected");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to get product name in PGP Page
	 */
	public String getProductName_PGP() {
		try {
			if (isElementPresent(title_selectedProduct_PGP)) {
				java.util.List<WebElement> productNames = new ArrayList<WebElement>();
				java.util.List<WebElement> allProducts = new ArrayList<WebElement>();
				productNames = findWebElements(title_selectedProduct_PGP);
				allProducts = findWebElements(getAllProducts);
				scrollToViewElement(allProducts.get(0));
				if (isElementDisplayed(title_selectedProduct_PGP)) {
					String value = productNames.get(0).getText();
					return value;
				} else {
					testStepFailed("Could not get the product name from PGP Page", "Element not displayed");
				}
			} else {
				testStepFailed("Could not get the product name from PGP Page", "Element not present");
			}
		} catch (Exception e) {
			testStepFailed("Could not get the product name from PGP Page");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Description: Method to select the second product on the page
	 */
	public void selectSecondProduct() {
		try {
			waitForElementToDisplay(getAllProducts, 10);
			if (isElementDisplayed(getAllProducts)) {
				java.util.List<WebElement> allProducts = new ArrayList<WebElement>();
				allProducts = findWebElements(getAllProducts);
				scrollToViewElement(allProducts.get(1));
				waitTime(3);
				allProducts.get(1).click();
				testStepInfo("The second product is clicked");
			} else {
				testStepFailed("Products were not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Second Product could not be selected");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Add to Cart
	 */
	public void clickAddToCart() {
		try {
			if (isElementDisplayed(addToCart)) {
				highLighterMethod(addToCart);
				clickOn(addToCart);
				waitForElementToDisplay(productAddedtoCartMessage, 100);
				testStepInfo("The add to cart button was clicked");
			} else {
				testStepFailed("The add to cart button was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Add to cart could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Add to Cart in PGP Page for first product.
	 */
	public void clickAddToCart_PGP() {
		try {
			if (isElementDisplayed(getAllProducts)) {
				java.util.List<WebElement> allProducts = new ArrayList<WebElement>();
				allProducts = findWebElements(getAllProducts);
				Actions action = new Actions(driver);
				WebElement target = allProducts.get(0);
				action.moveToElement(target).perform();
				if (isElementDisplayed(addToCart)) {
					java.util.List<WebElement> allProducts_addToCart = new ArrayList<WebElement>();
					allProducts_addToCart = findWebElements(addToCart);
					highLighterMethod(allProducts_addToCart.get(0));
					allProducts_addToCart.get(0).click();
					testStepInfo("The add to cart button of a product in PGP Page was clicked");

				} else {
					testStepFailed("The add to cart button of a product in PGP Page was not displayed");
				}
			}
		} catch (Exception e) {
			testStepFailed("Add to cart could not be clicked in the PGP Page");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Add to Comparison List in PGP Page for first
	 * product.
	 */
	public void clickAddToComparisonList_PGP() {
		try {
			if (isElementDisplayed(getAllProducts)) {
				java.util.List<WebElement> allProducts = new ArrayList<WebElement>();
				allProducts = findWebElements(getAllProducts);
				Actions action = new Actions(driver);
				WebElement target = allProducts.get(0);
				action.moveToElement(target).perform();
				if (isElementDisplayed(addToComparisonList)) {
					java.util.List<WebElement> allProducts_addToCart = new ArrayList<WebElement>();
					allProducts_addToCart = findWebElements(addToComparisonList);
					highLighterMethod(allProducts_addToCart.get(0));
					allProducts_addToCart.get(0).click();
					testStepInfo("The add to Comparison List button of a product in PGP Page was clicked");

				} else {
					testStepFailed("The add to Comparison List button of a product in PGP Page was not displayed");
				}
			}
		} catch (Exception e) {
			testStepFailed("Add to Comparison List - could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Add to Wish List in PGP Page for first
	 * product.
	 */
	public void clickAddToWishList_PGP() {
		try {
			if (isElementDisplayed(getAllProducts)) {
				java.util.List<WebElement> allProducts = new ArrayList<WebElement>();
				allProducts = findWebElements(getAllProducts);
				Actions action = new Actions(driver);
				WebElement target = allProducts.get(0);
				action.moveToElement(target).perform();
				if (isElementDisplayed(addToWishList)) {
					java.util.List<WebElement> allProducts_addToCart = new ArrayList<WebElement>();
					allProducts_addToCart = findWebElements(addToWishList);
					highLighterMethod(allProducts_addToCart.get(0));
					allProducts_addToCart.get(0).click();
					testStepInfo("The add to Wish List button of a product in PGP Page was clicked");

				} else {
					testStepFailed("The add to Wish List button of a product in PGP Page was not displayed");
				}
			}
		} catch (Exception e) {
			testStepFailed("Add to Wish List - could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Add to Comparison List in PDP Page for first
	 * product.
	 */
	public void clickAddToComparisonList_PDP() {
		try {
			if (isElementDisplayed(addToComparisonList)) {
				highLighterMethod(addToComparisonList);
				clickOn(addToComparisonList);
			} else {
				testStepFailed("The add to Comparison List button of a product in PDP Page was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Add to Comparison List - could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Add to Wish List in PDP Page for first
	 * product.
	 */
	public void clickAddToWishList_PDP() {
		try {
			if (isElementDisplayed(addToWishList)) {
				highLighterMethod(addToWishList);
				clickOn(addToWishList);
			} else {
				testStepFailed("The add to Wish List button of a product in PDP Page was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Add to Wish List - could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on the Shopping Cart Link
	 */
	public void clickShoppingCartLink() {
		try {
			if (isElementDisplayed(shoppingCartLink)) {
				highLighterMethod(shoppingCartLink);
				clickOn(shoppingCartLink);
				testStepInfo("The Shopping Cart Link was clicked");
				GOR.productAdded = true;
			} else {
				testStepFailed("The Shopping Cart Link was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Shopping Cart Link could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on the Shopping Cart Link
	 */
	public void clickWishListLink() {
		try {
			if (isElementDisplayed(shoppingCartLink)) {
				highLighterMethod(shoppingCartLink);
				clickOn(shoppingCartLink);
				testStepInfo("The Shopping Cart Link was clicked");
			} else {
				testStepFailed("The Shopping Cart Link was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Shopping Cart Link could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to get title of the product after selecting it from PGP
	 * Page.
	 */
	public String getProductTitle_PDP() {
		try {
			if (isElementDisplayed(title_selectedProduct_PDP)) {
				highLighterMethod(title_selectedProduct_PDP);
				return getText(title_selectedProduct_PDP);
			} else {
				testStepFailed("Product Title of the selected product not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not get the product title");
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * Description: Method to update product quantity.
	 */
	public void updateProductQuantity(int quantity) {
		try {
			if (isElementDisplayed(quantityUpdateBox)) {
				highLighterMethod(quantityUpdateBox);
				typeIn(quantityUpdateBox, String.valueOf(quantity));
			} else {
				testStepFailed("Could not update the quantity of the product in the PDP page",
						"Quantity update box not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not update the quantity of the product in the PDP page");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to get product price.
	 */
	public float getProductPrice() {
		try {
			waitForElementToDisplay(productPrice, 120);
			if (isElementDisplayed(productPrice)) {
				highLighterMethod(productPrice);
				String price = getText(productPrice).substring(1);
				float productPrice = Float.parseFloat(price);
				return productPrice;
			} else {
				testStepFailed("Could not get the price of the product in the PDP page", "Product Price not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not get the price of the product in the PDP page");
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Description: Method to click on the Comparison List Link
	 */
	public void clickComparisonListLink() {
		try {
			waitForElementToDisplay(comparsionListLink, 30);
			if (isElementDisplayed(comparsionListLink)) {
				highLighterMethod(comparsionListLink);
				clickOn(comparsionListLink);
				testStepInfo("The Comparison List Link was clicked");
			} else {
				testStepFailed("The Comparison List Link was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Comparison List Link could not be clicked");
			e.printStackTrace();
		}
	}
}