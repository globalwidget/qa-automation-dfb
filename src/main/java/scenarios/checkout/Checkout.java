package scenarios.checkout;

import java.util.HashMap;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.MiniCart;
import pages.ShoppingCart;
import pages.SignIn;

public class Checkout extends ApplicationKeywords {
	BaseClass obj;
	Cart_AllProducts cart_AllProducts;
	ShoppingCart shoppingCart;
	pages.Checkout checkout;
	HeaderAndFooters headerAndfooter;
	MiniCart miniCart;
	SignIn signIn;
	HomePage homePage;

	private boolean status = false;
	HashMap<String, String> billingData;
	HashMap<String, String> shippingData;

	public Checkout(BaseClass obj) {
		super(obj);
		this.obj = obj;
		cart_AllProducts = new Cart_AllProducts(obj);
		shoppingCart = new ShoppingCart(obj);
		checkout = new pages.Checkout(obj);
		headerAndfooter = new HeaderAndFooters(obj);
		miniCart = new MiniCart(obj);
		signIn = new SignIn(obj);
		homePage = new HomePage(obj);
	}

	/*
	 * TestCaseid : Checkout Description : To Verify if the user is not able to
	 * place the order with any invalid details in the checkout page.
	 */
	public void checkout_InvalidShippingDetails() {
		try {
			String invalidEmailAddress = retrieve("invalidEmailAddress");
			String invalidShippingPostCode = retrieve("invalidShippingPostCode");

			shippingData = new HashMap<String, String>();
			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("company_data", retrieve("company"));
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
			shippingData.put("postCode_data", invalidShippingPostCode);
			shippingData.put("email_data", invalidEmailAddress);

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickShoppingCartLink();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.fillDetailsShipping(shippingData);
			checkout.verifyRelevantMessage("Invalid Postal Code");
			checkout.verifyRelevantMessage("Invalid Email");
			checkout.clickNext();
			checkout.verifyRelevantMessage("Missing Shipping Method");
			checkout.ClearDetailsShipping();
			checkout.clickDesiredShippingOption("Overnight");
			checkout.clickNext();
			checkout.VerifyEmptyFieldErrors(true);
			headerAndfooter.clickLogo();
		} catch (Exception e) {
			testStepFailed("Placing Order with Invalid Shipping Details could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Verify if the user is able to fill shipping details successfully
	 * and suggested address is displayed.
	 */
	public void checkout_ValidShippingDetails() {
		try {
			shippingData = new HashMap<String, String>();
			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("company_data", retrieve("company"));
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
			shippingData.put("postCode_data", retrieve("postCode"));
			shippingData.put("email_data", retrieve("email"));

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickShoppingCartLink();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.fillDetailsShipping(shippingData);
//			checkout.verifyRelevantMessage("Suggested Address");
			checkout.clickDesiredShippingOption("Overnight");
			checkout.clickNext();
			checkout.verifyRelevantMessage("Review and Payments");
			headerAndfooter.clickLogo();

		} catch (Exception e) {
			testStepFailed(
					"Successful filling of valid shipping details and display of suggested address could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : DFB_TS_065 Description :Verify if the user is able to choose the
	 * shipping method and total price should be calculated based on the selection.
	 */
	public void verifyPriceForShippingMethods() {
		try {

			shippingData = new HashMap<String, String>();
			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("company_data", retrieve("company"));
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
			shippingData.put("postCode_data", retrieve("postCode"));
			shippingData.put("email_data", retrieve("email"));

			float price_first = 0;
			float price_second;
			float totalProductPrice = 0;

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndfooter.goTo_Products(0, 0);
			cart_AllProducts.selectFirstProduct();
			price_first = cart_AllProducts.getProductPrice();
			totalProductPrice = price_first * 2;
			cart_AllProducts.clickAddToCart();
			waitTime(4);
			headerAndfooter.clickOnMiniCart();
			miniCart.updateCartQuantity(2);
			miniCart.clickUpdateProductCountIcon();
			miniCart.clickGoToCheckout();
			checkout.fillDetailsShipping(shippingData);
			checkout.clickDesiredShippingOption("Standard");
			checkout.clickNext();
			price_second = checkout.getShippingPrice();
			checkout.checkOrderTotal(totalProductPrice, price_second);
			checkout.clickShippingProgressBar();
			checkout.clickDesiredShippingOption("Overnight");
			checkout.clickNext();
			price_second = checkout.getShippingPrice();
			checkout.checkOrderTotal(totalProductPrice, price_second);
			headerAndfooter.clickLogo();
			waitTime(3);
			headerAndfooter.clickOnMiniCart();
			miniCart.updateCartQuantity(6);
			miniCart.clickUpdateProductCountIcon();
			miniCart.clickGoToCheckout();
			totalProductPrice = price_first * 6;
			checkout.clickDesiredShippingOption("FreeStandard");
			checkout.clickNext();
			price_second = checkout.getShippingPrice();
			checkout.checkOrderTotal(totalProductPrice, price_second);
			checkout.clickShippingProgressBar();
			checkout.clickDesiredShippingOption("Overnight");
			checkout.clickNext();
			price_second = checkout.getShippingPrice();
			checkout.checkOrderTotal(totalProductPrice, price_second);
			headerAndfooter.clickLogo();
			waitTime(3);
			headerAndfooter.clickOnMiniCart();
			miniCart.updateCartQuantity(10);
			miniCart.clickUpdateProductCountIcon();
			miniCart.clickGoToCheckout();
			totalProductPrice = price_first * 10;
			totalProductPrice = (float) (Math.round((totalProductPrice) * 10) / 10.0);
			checkout.clickDesiredShippingOption("FreeExpedited");
			checkout.clickNext();
			price_second = checkout.getShippingPrice();
			checkout.checkOrderTotal(totalProductPrice, price_second);
			checkout.clickShippingProgressBar();
			checkout.clickDesiredShippingOption("Overnight");
			checkout.clickNext();
			price_second = checkout.getShippingPrice();
			checkout.checkOrderTotal(totalProductPrice, price_second);
			headerAndfooter.clickLogo();
			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
		} catch (Exception e) {
			testStepFailed("Could not verify price updation for different shipping methods in Checkout Page");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| headerAndfooter.testFailure || checkout.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : CheckoutShipping_DFB_TS_066 Description : Verify login section
	 * appears below the email address if the user enters an existing account email.
	 */
	public void checkout_verifyLoginSectionInCheckout() {
		try {

			shippingData = new HashMap<String, String>();

			shippingData.put("email_data", retrieve("email"));

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.loggedIn == true) {
				headerAndfooter.navigateMyAccountMenu("signOut");
			}

			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickShoppingCartLink();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.fillDetailsShipping(shippingData);
			checkout.verifyLoginSection();
			headerAndfooter.clickLogo();

		} catch (Exception e) {
			testStepFailed("Login Section display in Checkout page could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : CheckoutShipping_DFB_TS_067 Description : Verify if the guest
	 * user is able to sign in on checkout page if they have registered account
	 * already.
	 */
	public void checkout_verifyLoginInCheckout() {
		try {

			shippingData = new HashMap<String, String>();

			shippingData.put("email_data", retrieve("email"));
			String password = retrieve("password");

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.loggedIn == true) {
				headerAndfooter.navigateMyAccountMenu("signOut");
			}

			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickShoppingCartLink();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.fillDetailsShipping(shippingData);
			checkout.login(password);
			headerAndfooter.clickLogo();
			headerAndfooter.navigateMyAccountMenu("signOut");

		} catch (Exception e) {
			testStepFailed("Login in Checkout page could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : CheckoutShipping_DFB_TS_068 Description : Verify if all the
	 * saved Address appears in the Shipping page.
	 */
	public void checkout_verifySavedAddressSectionInCheckout() {
		try {

			String email = retrieve("email");
			String password = retrieve("password");

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndfooter.goToSignIn();
				signIn.signIn(email, password);
			}

			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickShoppingCartLink();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.goTo_Home();
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.verifySavedAddressSection();
			headerAndfooter.clickLogo();
			headerAndfooter.navigateMyAccountMenu("signOut");
			waitTime(3);
		} catch (Exception e) {
			testStepFailed("Saved Address Section display in Checkout page could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || signIn.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : CheckoutShipping_DFB_TS_069 Description : Verify if the user is
	 * able to add a new Address.
	 */
	public void checkout_verifyNewAddressSectionInCheckout() {
		try {

			shippingData = new HashMap<String, String>();
			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("company_data", retrieve("company"));
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
			shippingData.put("postCode_data", retrieve("postCode"));
			shippingData.put("email_data", null);
			String email = retrieve("emailForLogin");
			String password = retrieve("password");

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndfooter.goToSignIn();
				signIn.signIn(email, password);
			}

			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickShoppingCartLink();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.clickNewAddressButton();
			checkout.fillDetailsShipping(shippingData);
			checkout.clickSaveAddressCheckboxShipping();
			checkout.clickShipHereButton();
			checkout.verifyNewAddresBlock(shippingData);
			headerAndfooter.clickLogo();
			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndfooter.navigateMyAccountMenu("signOut");
		} catch (Exception e) {
			testStepFailed("Saved Address Section display in Checkout page could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || signIn.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : CheckoutShipping_DFB_TS_084 Description : To Verify if the user
	 * is not able to place the order with any invalid card details in the checkout
	 * page.
	 */
	public void checkout_InvalidCreditCardDetails() {
		try {
			String invalidcardNumber = retrieve("invalidCardNumber");
			String invalidcardVerificationNumber = retrieve("invalidCardVerificationNumber");
			String cardVerificationNumber = retrieve("cardVerificationNumber");
			String doesNotMatchType = retrieve("doesNotMatchType");
			String cardNumber = retrieve("cardNumber");
			String expirationMonth = retrieve("expirationMonth");
			String expirationYear = retrieve("expirationYear");
			String invalidExpirationYear = retrieve("invalidExpirationYear");

			shippingData = new HashMap<String, String>();
			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("company_data", retrieve("company"));
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
			shippingData.put("postCode_data", retrieve("postCode"));
			shippingData.put("email_data", retrieve("email"));

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickShoppingCartLink();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.fillDetailsShipping(shippingData);
			checkout.clickDesiredShippingOption("Overnight");
			checkout.clickNext();
//			checkout.selectCreditCardPayment();
//			checkout.clickPlaceOrder();
//			checkout.VerifyEmptyCardDetailsErrors();
			checkout.fillDetailsforCreditCard(invalidcardNumber, expirationMonth, invalidExpirationYear,
					invalidcardVerificationNumber);
			checkout.verifyRelevantMessage("Invalid Card Number");
			checkout.fillDetailsforCreditCard(doesNotMatchType, null, null, null);
			checkout.verifyRelevantMessage("Card Number does not match type");
			checkout.fillDetailsforCreditCard(cardNumber, null, null, null);
//			checkout.verifyRelevantMessage("Invalid expiry date");
			checkout.fillDetailsforCreditCard(null, null, expirationYear, null);
			checkout.verifyRelevantMessage("Invalid Verification Number");
			checkout.fillDetailsforCreditCard(null, null, null, cardVerificationNumber);
//			checkout.clickTermsCheckbox();
			checkout.clickPlaceOrder();
			checkout.verifyRelevantMessage("terms unchecked error");
			headerAndfooter.clickLogo();

		} catch (Exception e) {
			testStepFailed("Error messages for Invalid Card Details could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : CheckoutPayments_MHN_TS_071 Description : Verify the given
	 * shipping address are getting displayed when the
	 * "My billing and shipping address are the same" check box is selected.
	 */
	public void checkout_verifyBillingAddressAutofilledSameAsShippingAddress() {
		try {
			shippingData = new HashMap<String, String>();
			String firstName_data = retrieve("firstName_shipping");
			String lastName_data = retrieve("lastName_shipping");
			String company_data = retrieve("company");
			String billingCountry_data = "United States";
			String streetAddressFirstLine_data = retrieve("streetAddressFirstLine_shipping");
			String streetAddressSecondLine_data = retrieve("streetAddressSecondLine_shipping");
			String streetAddressThirdLine_data = retrieve("streetAddressThirdLine_shipping");
			String city_data = retrieve("city_shipping");
			String state_data = retrieve("state_shipping");
			String phoneNumber_data = retrieve("phoneNumber");
			String postCode_data = retrieve("postCode");
			String email_data = retrieve("email");

			shippingData.put("firstName_data", firstName_data);
			shippingData.put("lastName_data", lastName_data);
			shippingData.put("company_data", company_data);
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", streetAddressFirstLine_data);
			shippingData.put("streetAddressSecondLine_data", streetAddressSecondLine_data);
			shippingData.put("streetAddressThirdLine_data", streetAddressThirdLine_data);
			shippingData.put("city_data", city_data);
			shippingData.put("state_data", state_data);
			shippingData.put("phoneNumber_data", phoneNumber_data);
			shippingData.put("postCode_data", postCode_data);
			shippingData.put("email_data", email_data);

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickShoppingCartLink();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.fillDetailsShipping(shippingData);
			checkout.clickDesiredShippingOption("Overnight");
			checkout.clickNext();
//			checkout.selectCreditCardPayment();
			checkout.verifyDataInBillingAddress(firstName_data, lastName_data, streetAddressFirstLine_data,
					streetAddressSecondLine_data, streetAddressThirdLine_data, city_data, state_data,
					Integer.parseInt(postCode_data), billingCountry_data, phoneNumber_data);
			headerAndfooter.clickLogo();

		} catch (Exception e) {
			testStepFailed(
					"The given Shipping Address getting displayed when the \"My billing and shipping address are the same\" check box is selected could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : CheckoutPayments_MHN_TS_072 Description : Verify the billing
	 * address form is getting displayed when the
	 * "My billing and shipping address are the same" check box is not selected.
	 */
	public void checkout_verifyBillingFormDisplayed() {
		try {
			shippingData = new HashMap<String, String>();
			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("company_data", retrieve("company"));
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
			shippingData.put("postCode_data", retrieve("postCode"));
			shippingData.put("email_data", retrieve("email"));

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickShoppingCartLink();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.fillDetailsShipping(shippingData);
			checkout.clickDesiredShippingOption("Overnight");
			checkout.clickNext();
//			checkout.selectCreditCardPayment();
			checkout.clickBillingShippingAddressSame();
			checkout.verifyBillingFormDisplayed();
			headerAndfooter.clickLogo();
		} catch (Exception e) {
			testStepFailed("Billing Address form not displayed when relevant check box is not selected");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : CheckoutPayments_MHN_TS_074 Description : To Verify validation
	 * error message is displayed upon proceeding with any of the invalid billing
	 * address details.
	 */
	public void checkout_InvalidBillingDetails() {
		try {
			String invalidBillingPostCode = retrieve("invalidBillingPostCode");

			shippingData = new HashMap<String, String>();
			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("company_data", retrieve("company"));
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
			shippingData.put("postCode_data", retrieve("postCode"));
			shippingData.put("email_data", retrieve("email"));

			billingData = new HashMap<String, String>();
			billingData.put("firstName_data", retrieve("firstName_billing"));
			billingData.put("lastName_data", retrieve("lastName_billing"));
			billingData.put("company_data", retrieve("company_billing"));
			billingData.put("billingCountry_data", null);
			billingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_billing"));
			billingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_billing"));
			billingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_billing"));
			billingData.put("city_data", retrieve("city_billing"));
			billingData.put("state_data", retrieve("state_billing"));
			billingData.put("phoneNumber_data", retrieve("phoneNumber_billing"));
			billingData.put("postCode_data", invalidBillingPostCode);

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickShoppingCartLink();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.fillDetailsShipping(shippingData);
			checkout.clickDesiredShippingOption("Overnight");
			checkout.clickNext();
//			checkout.selectCreditCardPayment();
			checkout.clickBillingShippingAddressSame();
			checkout.clickUpdate();
			checkout.VerifyEmptyFieldErrors(false);
			checkout.fillDetailsBilling(billingData);
			checkout.verifyRelevantMessage("Invalid Postal Code");
			headerAndfooter.clickLogo();
		} catch (Exception e) {
			testStepFailed("Placing Order with Invalid Billing Details could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : CheckoutPayments_DFB_TS_073 Description : To Verify if the user
	 * is able to fill the billing address with all valid details.
	 */
	public void checkout_ValidBillingDetails() {
		try {
			shippingData = new HashMap<String, String>();
			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("company_data", retrieve("company"));
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
			shippingData.put("postCode_data", retrieve("postCode"));
			shippingData.put("email_data", retrieve("email"));

			billingData = new HashMap<String, String>();
			billingData.put("firstName_data", retrieve("firstName_billing"));
			billingData.put("lastName_data", retrieve("lastName_billing"));
			billingData.put("company_data", retrieve("company_billing"));
			billingData.put("billingCountry_data", null);
			billingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_billing"));
			billingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_billing"));
			billingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_billing"));
			billingData.put("city_data", retrieve("city_billing"));
			billingData.put("state_data", retrieve("state_billing"));
			billingData.put("phoneNumber_data", retrieve("phoneNumber_billing"));
			billingData.put("postCode_data", retrieve("billingPostCode"));

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickShoppingCartLink();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.fillDetailsShipping(shippingData);
			checkout.clickDesiredShippingOption("Overnight");
			checkout.clickNext();
//			checkout.selectCreditCardPayment();
			checkout.clickBillingShippingAddressSame();
			checkout.fillDetailsBilling(billingData);
			checkout.clickUpdate();
			headerAndfooter.clickLogo();
		} catch (Exception e) {
			testStepFailed("Successful entry of Valid Billing Details could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : CheckoutPayments_DFB_TS_075 Description : To Verify if the user
	 * is able to update the address when the
	 * "My billing and shipping address are the same" check box is not selected.
	 */
	public void checkout_NewBillingDetails() {
		try {

			String email = retrieve("email");
			String password = retrieve("password");

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndfooter.goToSignIn();
				signIn.signIn(email, password);
			}

			billingData = new HashMap<String, String>();
			billingData.put("firstName_data", retrieve("firstName_billing"));
			billingData.put("lastName_data", retrieve("lastName_billing"));
			billingData.put("company_data", retrieve("company_billing"));
			billingData.put("billingCountry_data", "United States");
			billingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_billing"));
			billingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_billing"));
			billingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_billing"));
			billingData.put("city_data", retrieve("city_billing"));
			billingData.put("state_data", retrieve("state_billing"));
			billingData.put("phoneNumber_data", retrieve("phoneNumber_billing"));
			billingData.put("postCode_data", retrieve("billingPostCode"));

			headerAndfooter.goTo_Home();
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickShoppingCartLink();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.clickDesiredShippingOption("Overnight");
			checkout.clickNext();
//			checkout.selectCreditCardPayment();
			checkout.clickBillingShippingAddressSame();
			checkout.selectNewAddressFromDropdown();
			checkout.fillDetailsBilling(billingData);
			checkout.clickSaveAddressCheckboxBilling();
			checkout.clickUpdate();
			checkout.verifyDataInBillingAddress(billingData.get("firstName_data"), billingData.get("lastName_data"),
					billingData.get("streetAddressFirstLine_data"), billingData.get("streetAddressSecondLine_data"),
					billingData.get("streetAddressThirdLine_data"), billingData.get("city_data"),
					billingData.get("state_data"), Integer.parseInt(billingData.get("postCode_data")),
					billingData.get("billingCountry_data"), billingData.get("phoneNumber_data"));
			headerAndfooter.clickLogo();
			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndfooter.navigateMyAccountMenu("signOut");
		} catch (Exception e) {
			testStepFailed("Successful entry of Valid Billing Details could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}
//
//	/*
//	 * TestCaseid : Checkout_MHN_TS_058 Description : To Verify if the user is able
//	 * to navigate to Checkout - Shipping page from Review & Payments page.
//	 */
//	public void checkout_verifyNavigationToShippingPageFromPayments() {
//		try {
//			shippingData = new HashMap<String, String>();
//			shippingData.put("firstName_data", retrieve("firstName_shipping"));
//			shippingData.put("lastName_data", retrieve("lastName_shipping"));
//			shippingData.put("company_data", retrieve("company"));
//			shippingData.put("billingCountry_data", null);
//			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
//			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
//			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
//			shippingData.put("city_data", retrieve("city_shipping"));
//			shippingData.put("state_data", retrieve("state_shipping"));
//			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
//			shippingData.put("postCode_data", retrieve("postCode"));
//			shippingData.put("email_data", retrieve("email"));
//
//	headerAndfooter.goTo_Home();
//
//	if(GOR.OfferPopUpHandled==false)
//
//	{
//		homePage.closeOfferPopup();
//	}
//			if (GOR.productAdded == false) {
//				headerAndfooter.goTo_Products(0, 0);
//				cart_AllProducts.selectFirstProduct();
//				cart_AllProducts.clickAddToCart();
//				cart_AllProducts.clickShoppingCartLink();
//				shoppingCart.clickProceedToCheckOut();
//			} else {
//				headerAndfooter.clickOnMiniCart();
//				miniCart.clickGoToCheckout();
//			}
//			checkout.fillDetailsShipping(shippingData);
//			checkout.clickDesiredShippingOption("Overnight");
//			checkout.clickNext();
//			checkout.clickShippingProgressBar();
//			checkout.verifyShippingHeader();
//			testStepInfo("Successfully navigated from Payments Tab to Shipping Tab");
//		} catch (Exception e) {
//			testStepFailed("Navigation to Shipping Tab from Payments could not be verified");
//		}
//		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
//				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
//			status = true;
//		}
//		this.testFailure = status;
//	}

	/*
	 * TestCaseid : CheckoutPayments_MHN_TS_078_079 Description : To validate error
	 * message is displayed upon submitting invalid coupon and if user is able to
	 * apply valid coupon in the checkout.
	 */
	public void checkout_ApplyInvalidAndValidCoupon() {
		try {

			String invalidCouponCode = retrieve("invalidCouponCode");
			String validCouponCode = retrieve("validCouponCode");
			shippingData = new HashMap<String, String>();
			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("company_data", retrieve("company"));
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
			shippingData.put("postCode_data", retrieve("postCode"));
			shippingData.put("email_data", retrieve("email"));

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickShoppingCartLink();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.fillDetailsShipping(shippingData);
			checkout.clickDesiredShippingOption("Overnight");
			checkout.clickNext();
			checkout.clickApplyDiscountCodeHeader();
			checkout.clickApplyDiscount();
			checkout.verifyRelevantMessage("Discount Code Missing");
			checkout.enterDiscountCode(invalidCouponCode);
			checkout.clickApplyDiscount();
			checkout.verifyRelevantMessage("Invalid Coupon");
			checkout.enterDiscountCode(validCouponCode);
			checkout.clickApplyDiscount();
			checkout.clickCancel_DiscountCoupon();
			testStepInfo("Valid Coupon was applied successfully");
			headerAndfooter.clickLogo();
		} catch (Exception e) {
			testStepFailed(
					"Could not verify errors for invalid coupon data and successful applying of valid coupon data");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : CheckoutPayments_MHN_TS_086 Description : To Verify if the user
	 * is able to place an oder by using valid credit card details as a signed in
	 * user.
	 */
	public void checkout_CompleteCheckoutWithValidDetails_SignedInUser() {
		try {
			String cardNumber = retrieve("cardNumber");
			String expirationMonth = retrieve("expirationMonth");
			String expirationYear = retrieve("expirationYear");
			String cardVerificationNumber = retrieve("cardVerificationNumber");
			String email = retrieve("emailForLogin");
			String password = retrieve("password");
			shippingData = new HashMap<String, String>();
			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("company_data", retrieve("company"));
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
			shippingData.put("postCode_data", retrieve("postCode"));
			shippingData.put("email_data", retrieve("email"));

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndfooter.goToSignIn();
				signIn.signIn(email, password);
			}

			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickShoppingCartLink();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
//			checkout.fillDetailsShipping(shippingData);
			checkout.clickDesiredShippingOption("Overnight");
			checkout.clickNext();
//			checkout.selectCreditCardPayment();
			checkout.fillDetailsforCreditCard(cardNumber, expirationMonth, expirationYear, cardVerificationNumber);
			checkout.clickTermsCheckbox();
			checkout.clickPlaceOrder();
			checkout.verifySuccessMessage();
			headerAndfooter.goTo_Home();
			headerAndfooter.navigateMyAccountMenu("signOut");

		} catch (Exception e) {
			testStepFailed(
					"Checkout of order with valid credit card details for a signed-in user could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure || signIn.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : CheckoutPayments_MHN_TS_087 Description : To Verify if the user
	 * is able to checkout the products with valid credit card details as a guest
	 * user.
	 */
	public void checkout_CompleteCheckoutWithValidDetails_GuestUser() {
		try {
			String cardNumber = retrieve("cardNumber");
			String expirationMonth = retrieve("expirationMonth");
			String expirationYear = retrieve("expirationYear");
			String cardVerificationNumber = retrieve("cardVerificationNumber");
			shippingData = new HashMap<String, String>();
			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("company_data", retrieve("company"));
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
			shippingData.put("postCode_data", retrieve("postCode"));
			shippingData.put("email_data", retrieve("email"));

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickShoppingCartLink();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.fillDetailsShipping(shippingData);
			checkout.clickDesiredShippingOption("Overnight");
			checkout.clickNext();
//			checkout.selectCreditCardPayment();
			checkout.fillDetailsforCreditCard(cardNumber, expirationMonth, expirationYear, cardVerificationNumber);
			checkout.clickTermsCheckbox();
			checkout.clickPlaceOrder();
			checkout.verifySuccessMessage();

		} catch (Exception e) {
			testStepFailed("Checkout of order with valid credit card details for a guest user could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}
}
