package scenarios.Register;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.CreateAccount;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.MyAccount;

public class Register extends ApplicationKeywords {
	BaseClass obj;
	CreateAccount register;
	HeaderAndFooters headerAndFooters;
	HomePage homepage;
	MyAccount myAccount;
	private boolean status = false;

	public Register(BaseClass obj) {
		super(obj);
		register = new CreateAccount(obj);
		headerAndFooters = new HeaderAndFooters(obj);
		homepage = new HomePage(obj);
		myAccount = new MyAccount(obj);
		this.obj = obj;
	}

	/*
	 * TestCaseid : Register_DFB_TS_005 Description : To Verify if the user is able
	 * to create a new account with valid details.
	 */
	public void register_ValidDetails() {
		try {
			String firstName = retrieve("firstName");
			String lastName = retrieve("lastName");
			String email = retrieve("email");
			String password = retrieve("password");

			headerAndFooters.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homepage.closeOfferPopup();
			}

			headerAndFooters.goToCreateAnAccount();
			register.register_FillDetails(firstName, lastName, email, password, password);
			register.clickCreateAccount();
			register.verifyMessage_Register("successMessage");
			headerAndFooters.navigateMyAccountMenu("signOut");
		} catch (Exception e) {
			testStepFailed("Creating an account using valid credentials could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Register_DFB_TS_006 Description : To Verify if the user is not
	 * able to create a new account with invalid details.
	 */
	public void register_InvalidDetails() {
		try {
			String firstName = retrieve("firstName");
			String lastName = retrieve("lastName");
			String email = retrieve("email");
			String invalidEmail = retrieve("invalidEmail");
			String passwordWrongLength = retrieve("passwordWrongLength");
			String passwordWrongComposition = retrieve("passwordWrongComposition");
			String password = retrieve("password");
			String wrongPassword = retrieve("wrongPassword");

			headerAndFooters.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homepage.closeOfferPopup();
			}

			headerAndFooters.goToCreateAnAccount();
			register.clickCreateAccount();
			register.VerifyEmptyFieldErrors();
			register.register_FillDetails(firstName, lastName, invalidEmail, passwordWrongLength, null);
			register.verifyMessage_Register("passwordInvalidLength");
			register.register_FillDetails(null, null, null, passwordWrongComposition, wrongPassword);
			register.verifyMessage_Register("passsword_InvalidError_MinimumDifferentClasses");
			register.register_FillDetails(null, null, null, password, null);
			register.clickCreateAccount();
			register.verifyMessage_Register("emailInvalid");
			register.verifyMessage_Register("confirmPasswordInvalid");
			register.register_FillDetails(null, null, email, null, password);
			register.clickCreateAccount();
			register.verifyMessage_Register("accountExistsError");

		} catch (Exception e) {
			testStepFailed("Error messages for account registration using Invalid details could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Register_DFB_TS_010 Description : To Verify if the user is able
	 * sign up for Newsletter while creating an account.
	 */
	public void createAccount_VerifySignUpForNewsletter() {
		try {
			String firstName = retrieve("firstName");
			String lastName = retrieve("lastName");
			String email = retrieve("email");
			String password = retrieve("password");

			headerAndFooters.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homepage.closeOfferPopup();
			}

			headerAndFooters.goToCreateAnAccount();
			register.clickNewsletterCheckbox();
			register.register_FillDetails(firstName, lastName, email, password, password);
			register.clickCreateAccount();
			register.verifyMessage_Register("successMessage");
			myAccount.verifyMessage_MyAccount("newsLetterSubscribedMessage");

		} catch (Exception e) {
			testStepFailed("Creating an account along with subscribing to newsletter could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
