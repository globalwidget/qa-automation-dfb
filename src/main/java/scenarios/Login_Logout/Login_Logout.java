package scenarios.Login_Logout;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.SignIn;

public class Login_Logout extends ApplicationKeywords {
	BaseClass obj;
	SignIn signIn;
	HeaderAndFooters headerAndFooters;
	HomePage homepage;
	private boolean status = false;

	public Login_Logout(BaseClass obj) {
		super(obj);
		signIn = new SignIn(obj);
		headerAndFooters = new HeaderAndFooters(obj);
		homepage = new HomePage(obj);
		this.obj = obj;
	}

	/*
	 * TestCaseid : Login_DFB_TS_007 Description : To Verify if the user is able to
	 * login to Defense Boost application with valid credentials.
	 */
	public void login_ValidDetails() {
		try {

			String email = retrieve("email");
			String password = retrieve("password");

			if (GOR.OfferPopUpHandled == false) {
				homepage.closeOfferPopup();
			}

			headerAndFooters.goTo_Home();
			headerAndFooters.goToSignIn();
			signIn.signIn(email, password);
		} catch (Exception e) {
			testStepFailed("Login using valid credentials could not bbe verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || signIn.testFailure || homepage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Login_DFB_TS_008 Description : To Verify if the user is not able
	 * to create a new account with invalid details.
	 */
	public void login_InvalidDetails() {
		try {
			String invalidEmail = retrieve("invalidEmail");
			String invalidPassword = retrieve("invalidPassword");
			String email = retrieve("email");

			if (GOR.OfferPopUpHandled == false) {
				homepage.closeOfferPopup();
			}

			headerAndFooters.goTo_Home();
			headerAndFooters.goToSignIn();
			signIn.signIn("", "");
			signIn.verifyErrorMessage_SignIn("emailEmpty");
			signIn.verifyErrorMessage_SignIn("passwordEmpty");
			signIn.signIn(invalidEmail, invalidPassword);
			signIn.verifyErrorMessage_SignIn("emailInvalid");
			signIn.signIn(email, invalidPassword);
			signIn.verifyErrorMessage_SignIn("invalidCredentialsError");

		} catch (Exception e) {
			testStepFailed("Error message for Login using Invalid Credentials could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || signIn.testFailure || homepage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Logout_DFB_TS_011 Description : To Verify if the user is able to
	 * logout in Defense Boost application.
	 */
	public void logout() {
		try {

			String email = retrieve("email");
			String password = retrieve("password");

			if (GOR.OfferPopUpHandled == false) {
				homepage.closeOfferPopup();
			}
			if (GOR.loggedIn == false) {
				headerAndFooters.goTo_Home();
				headerAndFooters.goToSignIn();
				signIn.signIn(email, password);
			}
			headerAndFooters.navigateMyAccountMenu("signOut");
			headerAndFooters.goToSignIn();
			headerAndFooters.verifyNavigation("SignInPage");
			testStepInfo("Successfully logged out from the user account");

		} catch (Exception e) {
			testStepFailed("Logout could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || signIn.testFailure || homepage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
